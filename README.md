# Scrapetest

Web scraper made using Selenium

Scraps Snowboard data from https://www.lobstersnowboards.com

# Setup

pip install -r requirements.txt

# Run

python lobster.scraper.py

