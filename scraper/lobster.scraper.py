
import selenium
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.webdriver.support import ui, expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException

import os
from os import environ
import json
import time
import csv
from pathlib import Path

from scrapetest.scraper.Snowboard import Snowboard
from scrapetest.scraper.utils import AnyEC
from scrapetest.scraper.exceptions import NoProductsError

URL = 'https://www.lobstersnowboards.com/shop/'
OUTPUT_FILE = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'data/snowboards.csv')
OUTPUT_FIELDS = ["Url", "Name", "Image", "Price", "Sizes"]
DEFAULT_REGION = 'United States'

class LobsterScraper(object):

    def __init__(self, cookie=None, driver=selenium.webdriver.Firefox, scroll_pause=0.1, scroll_increment=300,
                 timeout=10):
        self.driver = driver()
        self.scroll_pause = scroll_pause
        self.scroll_increment = scroll_increment
        self.timeout = timeout
        self.driver.get(URL)
        self.driver.set_window_size(1200, 1000)
        self.wait = ui.WebDriverWait(self.driver, 5)
        self.create_output_csv()

    def login(self):
        pass

    def create_output_csv(self):
        '''
        Method to create the output file if it does not exist and add headers
        :return:
        '''
        if Path(OUTPUT_FILE).is_file():
            self.output_file = open(OUTPUT_FILE, 'a')
            self.wr = csv.writer(self.output_file, dialect='excel')
        else:
            self.output_file = open(OUTPUT_FILE, 'w')
            self.wr = csv.writer(self.output_file, dialect='excel')
            self.wr.writerow(OUTPUT_FIELDS)


    def update_region(self, region=DEFAULT_REGION):
        '''
        Method to update the region to Default (United States)
        :param region: (str) Name of the defaul region
        :return: NA
        '''
        try:
            region_selector = self.driver.find_element_by_xpath('//button[@data-id="country_list"]')
            region_selector.click()
            region_input_elem = region_selector.find_element_by_xpath('..//div[@class="bootstrap-select-searchbox"]/input')
            region_input_elem.send_keys(region)
            region_input_elem.send_keys(Keys.ENTER)
        except Exception as e:
            print(str(e))


    def get_snowboard_links(self):
        '''
        Method to collect links of all the snowboards listed on the website
        :return: List of the links of all snowboards
        '''
        try:
            self.wait.until(
                EC.presence_of_element_located((By.ID, "boards_scrollto"))
            )
            snowboard_anchors = self.driver.find_elements_by_xpath('//div[contains(@class,"shop-special-additions") or contains(@id,"boards_scrollto")] //div[contains(@class, "product-grid")]/a')
            snowboard_links = [elem.get_attribute('href') for elem in snowboard_anchors]
        except Exception as e:
            current_region = self.driver.find_element_by_xpath('//button[@data-id="country_list"]').get_attribute('title')
            if current_region == 'Rest of the World':
                self.update_region()
                return self.get_snowboard_links()
            else:
                raise NoProductsError

        return snowboard_links


    def load_snowboard_page(self, url):
        '''
        Method to load the item (snowboard) page
        :param url: URL of the item
        :return: NA
        '''
        self.driver.get(url)
        try:
            self.wait.until(
                EC.presence_of_element_located((By.CLASS_NAME, "main-view"))
            )
        except TimeoutException as e:
            print("Took to long to load the page", e)


    def get_snowboard_info(self, url):
        '''
        Method to extract information about a Snowboard item from the item page
        :param url: URL of the item (snowboard) page
        :return: Dict with attributes extracted about a snowboard - ["url", "name", "image", "price", "sizes"]
        '''
        try:
            self.load_snowboard_page(url)
            snowboard_content = self.driver.find_element_by_tag_name(
                'body').get_attribute("outerHTML")
            snowboard_info = Snowboard(url, snowboard_content).to_dict()
            return snowboard_info
        except Exception as e:
            print(str(e))

    def get_html(self, url):
        self.load_snowboard_page(url)
        return self.driver.page_source

    def scrap_snowboards(self):
        '''
        Method to collect the details of all snowboards listed on the website and save in the CSV data/snowboards.csv
        :return: None
        '''
        try:
            snowboard_links = self.get_snowboard_links()
            for link in snowboard_links:
                snowboard_info = self.get_snowboard_info(link)
                self.save_to_csv(snowboard_info)
        except NoProductsError as e:
            print(e)

        self.quit()

    def save_to_csv(self, data):
        record = [data[col.lower()] for col in OUTPUT_FIELDS]
        self.wr.writerow(record)

    def __enter__(self):
        return self

    def __exit__(self, *args, **kwargs):
        self.quit()

    def quit(self):
        self.output_file.close()
        if self.driver:
            self.driver.quit()


if __name__ == "__main__":
    scraper = LobsterScraper()
    scraper.scrap_snowboards()
