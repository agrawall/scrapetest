from bs4 import BeautifulSoup
from pprint import pprint
from .utils import *
from urllib.parse import urlparse


class Snowboard(object):
    attributes = ['url', 'name', 'image', 'price', 'sizes']

    def __init__(self, url, body):
        self.url = url
        self.hostname = urlparse(self.url).hostname
        self.soup = BeautifulSoup(body, 'html.parser')


    @property
    def name(self):
        return text_or_default(self.soup, '.product-title')

    @property
    def price(self):
        return text_or_default(self.soup, '.product_price > h2')

    @property
    def sizes(self):
        options = all_or_default(self.soup, '.product_colors > div > div > div > ul > li > a > span')
        sizes = list(map(lambda i: i.text.split("-")[0], options))
        sizes.remove('Select')
        return sizes

    @property
    def image(self):
        try:
            return self.hostname + one_or_default(self.soup, '.img-responsive').attrs['src']
        except:
            return None


    def to_dict(self):
        info = {}
        info['url'] = self.url
        info['name'] = self.name
        info['image'] = self.image
        info['price'] = self.price
        info['sizes'] = self.sizes

        return info

    def __dict__(self):
        return self.to_dict()

    def __eq__(self, that):
        return self.to_dict() == that.to_dict()

