class AnyEC(object):
    def __init__(self, *args):
        self.ecs = args

    def __call__(self, driver):
        for fn in self.ecs:
            try:
                if fn(driver):
                    return True
            except:
                pass
        return False


def one_or_default(element, selector, default=None):
    try:
        el = element.select_one(selector)
        if not el:
            return default
        return element.select_one(selector)
    except Exception as e:
        return default


def text_or_default(element, selector, default=None):
    try:
        return element.select_one(selector).get_text().strip()
    except Exception as e:
        return default


def all_or_default(element, selector, default=[]):
    try:
        elements = element.select(selector)
        if len(elements) == 0:
            return default
        return element.select(selector)
    except Exception as e:
        return default


def get_info(element, mapping, default=None):
    return {key: text_or_default(element, mapping[key], default=default) for key in mapping}


